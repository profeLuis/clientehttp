﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace HttpClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSolicitar_Click(object sender, EventArgs e)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                
                client.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
                //HTTP GET
                var responseTask = client.GetAsync("/posts/1");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    var readTask = result.Content.ReadAsStringAsync();

                    readTask.Wait();

                    var mensajesRecibidos = readTask.Result;
                    
                    Mensaje mensaje = JsonConvert.DeserializeObject<Mensaje>(mensajesRecibidos.ToString());

                    txtRespuesta.Text = mensaje.body;
                }
            }
        }
    }
    public class Mensaje{
        public int userId;
        public int id;
        public string title;
        public string body;
    }
}
