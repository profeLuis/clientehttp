﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Vistas.Startup))]
namespace Vistas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
