﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vistas.Controllers
{

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Pizzas nuevaPizza = new Pizzas
            {
                nombrePizza = "mexicana",
                tamano = "familiar"
            };
            ViewData["pizza"] = nuevaPizza;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
    public class Pizzas
    {
        public string nombrePizza;
        public string tamano;
    }
}